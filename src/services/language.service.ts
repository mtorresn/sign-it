import { Injectable } from '@angular/core';
import { Language } from '../classes/language';
import { Storage } from '@ionic/storage';
import { ENV } from './env';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LanguageService {
  selectedLanguage: string;
  basePath: string = ENV['base_path'];
  languagesPath: string = ENV['languages'];

  constructor(private storage: Storage, private http: Http) {}

  getLanguages(): Promise<Language[]> {
    return this.http.get(this.basePath + this.languagesPath)
      .toPromise()
      .then(response =>
        response.json() as Language[])
      .catch(this.handleError);
  }

  getLanguageProperties(key:string): Promise<Language> {
    return this.getLanguages().then(languages => languages.find(language => language.key === key));
  }

  setLanguage(key: string): void {
    this.selectedLanguage = key;
    this.storage.set('language', key);
  }

  getLanguage(): string {
    return this.selectedLanguage;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}

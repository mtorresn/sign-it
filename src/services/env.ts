export const ENV: object = {
  'base_path': 'http://signit.dd:8083',
  'languages': '/languages',
  'signs': '/signs'
};

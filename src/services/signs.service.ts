import { Injectable } from '@angular/core';
import { Sign } from '../classes/sign';
import { ENV } from './env';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { LanguageService } from './language.service';

@Injectable()
export class SignsService {
  basePath: string = ENV['base_path'];
  signsPath: string = ENV['signs'];

  constructor(private http: Http, private LanguageService: LanguageService) {}

  searchSigns(): Promise<Sign[]> {
    const path: string = this.basePath + this.signsPath + '?language=' + this.LanguageService.getLanguage();
    return this.http.get(path)
      .toPromise()
      .then(response =>
        this.formatSigns(response.json()) as Sign[])
      .catch(this.handleError);
  }
  formatSigns(signs: any): Sign[]{
    const map: Sign[] = signs.map(sign => this.formatSign(sign));
    return map;
  }

  formatSign(sign: any): Sign {
    const categories: string[] = sign.categories_str.split(',');
    return new Sign(sign.name, sign.image, sign.description, sign.language, categories);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms'

import { MyApp } from './app.component';
import { SelectLanguage } from '../pages/select-language/select-language.component';
import { Search } from '../pages/search/search.component';
import { BackgroundCard } from '../components/background-card/background-card.component';
import { SignCard } from '../components/sign-card/sign-card.component';

import { LanguageService } from '../services/language.service';
import { SignsService } from '../services/signs.service';

@NgModule({
  declarations: [
    MyApp,
    SelectLanguage,
    BackgroundCard,
    SignCard,
    Search
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SelectLanguage,
    BackgroundCard,
    SignCard,
    Search
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LanguageService,
    SignsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'sign-card',
  templateUrl: './sign-card.template.html'
})
export class SignCard {
  @Input() name: string;
  @Input() description: string;
  @Input() image: string;
  @Input() basepath: string;
  @Input() language: string;
  @Input() categories: string[];
}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'background-card',
  templateUrl: './background-card.template.html'
})
export class BackgroundCard {
  @Input() name: string;
  @Input() initials: string;
  @Input() image: string;
  @Input() basepath: string;
}

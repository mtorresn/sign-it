export class Category {
  id: number;
  name: string;
  key: string;
}

export class Language {
  name: string;
  image: string;
  key: string;
}

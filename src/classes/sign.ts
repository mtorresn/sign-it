export class Sign {
  name: string;
  image: string;
  description: string;
  language: string;
  categories: string[];

  constructor(name: string, image: string, description: string, language: string, categories: string[]){
    this.name = name;
    this.image = image;
    this.description = description;
    this.language = language;
    this.categories = categories;
  }
}

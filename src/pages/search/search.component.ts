import { Component, OnInit, Input } from '@angular/core';
import { LanguageService } from '../../services/language.service';
import { SignsService } from '../../services/signs.service';
import { Sign } from '../../classes/sign';
import { ENV } from '../../services/env';

@Component({
  selector: 'search',
  templateUrl: './search.component.html'
})
export class Search implements OnInit{
  language: string;
  searchResult: Sign[];
  title: string = 'Search';
  basepath: string = ENV['base_path'];

  @Input() toSearch: string;

  constructor(private LanguageService: LanguageService, private SignsService: SignsService) {}

  ngOnInit(): void {
    this.language = this.LanguageService.getLanguage();
  }

  searchSigns(): void {
    this.SignsService.searchSigns().then(result => {
      this.searchResult = result;
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Language } from '../../classes/language';
import { LanguageService } from '../../services/language.service';
import { Search } from '../../pages/search/search.component';
import { ENV } from '../../services/env';

@Component({
  selector: 'select-language',
  templateUrl: './select-language.component.html'
})
export class SelectLanguage implements OnInit{
  languages: Language[];
  basePath: string = ENV['base_path'];
  title: string = 'Select Language';

  constructor(private LanguageService: LanguageService, public navCtrl: NavController) {}

  ngOnInit(): void {
    this.getLanguages();
  }
  getLanguages():void {
    this.LanguageService.getLanguages().then(languages => {
      this.languages = languages;
    });
  }
  setLanguage(key: string): void {
    this.LanguageService.setLanguage(key);
    this.navCtrl.push(Search);
  }
}
